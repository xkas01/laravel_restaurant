<?php

use App\Http\Controllers\Admin\AdminContoller;
use App\Http\Controllers\Admin\CategoryContoller;
use App\Http\Controllers\Admin\MenuContoller;
use App\Http\Controllers\Admin\ReservationContoller;
use App\Http\Controllers\Admin\TableContoller;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware(['auth', 'admin'])->name('admin.')->prefix('admin')->group(function () {
    Route::get('/', [AdminContoller::class, 'index'])->name('index');
    Route::resource('/categories', CategoryContoller::class);
    Route::resource('/menus', MenuContoller::class);
    Route::resource('/tables', TableContoller::class);
    Route::resource('/reservations', ReservationContoller::class);
});

/*Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});*/

require __DIR__ . '/auth.php';
